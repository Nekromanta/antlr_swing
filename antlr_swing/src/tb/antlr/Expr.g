grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | blok)+ EOF!
    ;
    
blok
    : BEGIN^ (stat | blok)* END!
    ;


stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    //| VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_stat NL -> if_stat

    | NL ->
    ;


if_stat
    : IF^ e1=cmp_expr THEN! e2=expr (ELSE! e3=expr)?
    ;

cmp_expr
    : expr
    ( GREATER^ expr
    | LOWER^ expr
    | EQUAL^ expr
    )
    ;
    
expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

IF
  : 'if'
  ; 

ELSE
  : 'else'
  ; 

THEN
  : 'then'
  ;

EQUAL : '==';
GREATER : '>';
LOWER : '<';

VAR :'var';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


BEGIN
  : '{'
  ;
END
  : '}'
  ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
