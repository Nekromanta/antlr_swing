package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols globalSymbols = new GlobalSymbols();
	private LocalSymbols localSymbols = new LocalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	
	protected Integer add(Integer x, Integer y)
	{
		return x+y;
	}
	
	protected Integer sub(Integer x, Integer y)
	{
		return x-y;
	}
	
	protected Integer mul(Integer x, Integer y)
	{
		return x*y;
	}
	
	protected Integer div(Integer x, Integer y)
	{
		if(y==0)
		{
			throw new ArithmeticException("division by zero");
		}
		return x/y;
	}
	
	protected Integer mod(Integer x, Integer y)
	{
		return x%y;
	}
	
	
	protected void decVar(String name)
	{
		globalSymbols.newSymbol(name);
	}
	
	protected void setVarVal(String name, Integer value)
	{
		try
		{
			globalSymbols.getSymbol(name);
		}
		catch(Exception e)
		{
			//jeśli zmienna nie istnieje to ją zadeklaruj
			decVar(name);
		}
		globalSymbols.setSymbol(name, value);
	}
	
	protected Integer getVarVal(String name)
	{
		return globalSymbols.getSymbol(name);
	}
	
	
	
	
	
	
	
}
