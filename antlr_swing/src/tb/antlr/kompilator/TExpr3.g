tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje;separator=\" \n\">
start:
<name;separator=\" \n\"> ";

zakres  : ^(BEGIN {enterScope();} (e+=zakres | e+=expr | d+=decl)* {leaveScope();}) -> blok(wyr={$e}, dekl={$d})
        ;

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr)  -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)  -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)  -> pomnoz(p1={$e1.st},p2={$e2.st}) 
        | ^(DIV   e1=expr e2=expr)  -> podziel(p1={$e1.st},p2={$e2.st}) 
        | ^(PODST i1=ID   e2=expr)  -> podstaw(p1={$i1.text},p2={$e2.st})
        | INT                       -> int(i={$INT.text})
        | ID                        -> wczytaj(p1={$ID.text})
        | ^(IF e1=expr e2=expr e3=expr?) {numer++;}  ->  if(e1={$e1.st},e2={$e2.st},e3={$e3.st},nr={numer.toString()})
        | ^(GREATER e1=expr e2=expr)-> greater(p1={$e1.st},p2={e2.st},nr={numer.toString()})
        | ^(LOWER e1=expr e2=expr)-> lower(p1={$e1.st},p2={e2.st},nr={numer.toString()})
        | ^(EQUAL e1=expr e2=expr)-> equal(p1={$e1.st},p2={e2.st},nr={numer.toString()})
    ;
    